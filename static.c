#include "common.h"

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int distribute(longint_t *a, longint_t *b)
{
    REQUIRE(a->len == b->len, "They should be equal");

    assert(a->len % (global.mpi_size - 1) == 0);
    int block_size = a->len / (global.mpi_size - 1);

    int collegues[2 * global.mpi_size];
    for(int i = 0; i < global.mpi_size; ++i) {
        collegues[i * 2]     = i - 1;
        collegues[i * 2 + 1] = i + 1;
    }
    collegues[0 * 2 + 0] = collegues[0 * 2 + 1] = 0; // root collegues
    collegues[1 * 2 + 0] = 0; // first worker previous collegue
    collegues[(global.mpi_size - 1) * 2 + 1] = 0; // last worker next collegue

    for(int i = 1; i < global.mpi_size; ++i) {
        MPI_Send(&block_size, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        MPI_Send(a->digits + block_size * (i - 1), block_size, MPI_INT, i, 0, MPI_COMM_WORLD);
        MPI_Send(b->digits + block_size * (i - 1), block_size, MPI_INT, i, 0, MPI_COMM_WORLD);
        MPI_Send(collegues + i * 2, 2, MPI_INT, i, 0, MPI_COMM_WORLD);
    }

    return block_size;
}

void collect(longint_t *out, int block_size)
{
    out->len = block_size * (global.mpi_size - 1);
    for(int i = 1; i < global.mpi_size; ++i) {
        MPI_Recv(out->digits + block_size * (i - 1), block_size, MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    int carry = 0;
    MPI_Recv(&carry, 1, MPI_INT, global.mpi_size - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(carry) {
        out->digits[out->len] = 1;
        out->len++;
    }
}

void work(void);

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &global.mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &global.mpi_size);

    if(0 == global.mpi_rank) {
        input_t input; parseCli(&input, &argc, &argv);

        double time = 0;
        time -= MPI_Wtime();

        int block_size = distribute(&input.a, &input.b);
        longint_t out;
        collect(&out, block_size);

        time += MPI_Wtime();

        char *result __attribute__((cleanup(strCleanup))) = toString(&out);

        FILE *output_file __attribute__((cleanup(fileCleanup))) = fopen(input.ouput_filename, "w");
        fprintf(output_file, "%s", result);

        printf("[TIME] %lf\n", time);
    } else {
        work();
    }

    MPI_Finalize();
    return 0;
}

void work(void)
{
    int block_size = 0;
    MPI_Recv(&block_size, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    int a_block[block_size], b_block[block_size];
    MPI_Recv(a_block, block_size, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(b_block, block_size, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    int collegues[2];
    MPI_Recv(collegues, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    int res_block[2][block_size];

    int do_both = ((DIGIT_CULL - 1) == a_block[0] + b_block[0]);
    int carry[2] = {0, 1};
    for(int i = 0; i < block_size; ++i) {
        int tmp = a_block[i] + b_block[i];

        res_block[0][i] = (tmp + carry[0]) % DIGIT_CULL;
        carry[0] = (tmp + carry[0]) / (DIGIT_CULL);
        
        if(!do_both) continue;
        
        res_block[1][i] = (tmp + carry[1]) % DIGIT_CULL;
        carry[1] = (tmp + carry[1]) / (DIGIT_CULL);
    }

    int res_block_id = 0;
    int collegue_carry = 0;
    if(0 != collegues[0]) {
        MPI_Recv(&collegue_carry, 1, MPI_INT, collegues[0], MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    if(collegue_carry) {
        res_block[0][0] += 1;
        if(do_both) {
            res_block_id = 1;
        }
    }
    if(0 != collegues[1]) {
        MPI_Send(&carry[res_block_id], 1, MPI_INT, collegues[1], 0, MPI_COMM_WORLD);
    }

    MPI_Send(res_block[res_block_id], block_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    if(0 == collegues[1]) {
        MPI_Send(&carry[res_block_id], 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
}
