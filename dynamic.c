#include "common.h"

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef TASK_SIZE
#   define TASK_SIZE 2
#endif

typedef enum {
    TASK,
    STOP,
    VACANT,
    RESULT
} tag_t;

void work(void);

void scheduler(longint_t *out, longint_t *a, longint_t *b)
{
    REQUIRE(a->len == b->len, "They should be equal");
    int dummy;

    MPI_Status status;

    int nTasks = a->len / TASK_SIZE + (a->len % TASK_SIZE > 0);
    int taskSizes[nTasks];
    for(int i = 0; i < nTasks; ++i) {
        taskSizes[i] = min(a->len, TASK_SIZE * (i + 1)) - TASK_SIZE * i;
    }

    int results[nTasks][2][TASK_SIZE];
    int carries[nTasks][2];

    int received = 0;
    int allreceived = 0;

    int stopped = 0;
    int allstopped = 0;

    int sent = 0;
    int allsent = 0;
    while(!(allsent && allreceived && allstopped)) {
        MPI_Recv(&dummy, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        int worker = status.MPI_SOURCE;
        if(status.MPI_TAG == VACANT) {
            if(allsent) {
                MPI_Send(&dummy, 1, MPI_INT, worker, STOP, MPI_COMM_WORLD);
                stopped++;
                if(stopped == global.mpi_size - 1) {
                    allstopped = 1;
                }
                continue;
            }
            MPI_Send(&sent, 1, MPI_INT, worker, TASK, MPI_COMM_WORLD);
            MPI_Send(taskSizes + sent, 1, MPI_INT, worker, TASK, MPI_COMM_WORLD);
            MPI_Send(a->digits + TASK_SIZE * sent, taskSizes[sent], MPI_INT, worker, TASK, MPI_COMM_WORLD);
            MPI_Send(b->digits + TASK_SIZE * sent, taskSizes[sent], MPI_INT, worker, TASK, MPI_COMM_WORLD);
            sent++;
            if(sent == nTasks) {
                allsent = 1;
            }
        } else if(status.MPI_TAG == RESULT) {
            int res_id = dummy;
            printf("RESULT id %d from %d\n", res_id, worker);
            MPI_Recv(results[res_id][0], taskSizes[res_id], MPI_INT, worker, RESULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(results[res_id][1], taskSizes[res_id], MPI_INT, worker, RESULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(carries[res_id], 2, MPI_INT, worker, RESULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            received++;
            if(received == nTasks) {
                allreceived = 1;
            }
        } 
    }

    memcpy(out->digits, results[0][0], taskSizes[0] * sizeof(*(out->digits)));
    int carry = carries[0][0];
    for(int i = 1; i < nTasks; ++i) {
        int size = taskSizes[i];
        int *src = results[i][0];
        src[0] += carry;
        if((DIGIT_CULL - 1) == *src) {
            src = results[i][1];
            carry = carries[i][1];
        } else {
            carry = carries[i][0];
        }

        memcpy(out->digits + TASK_SIZE * i, src, taskSizes[i] * sizeof(*(out->digits)));
    }

    out->len = a->len;
    if(carry) {
        out->digits[out->len++] = 1;
    }
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &global.mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &global.mpi_size);

    if(0 == global.mpi_rank) {
        input_t input; parseCli(&input, &argc, &argv);

        double time = 0;
        time -= MPI_Wtime();

        longint_t out;
        scheduler(&out, &input.a, &input.b);

        time += MPI_Wtime();

        char *result __attribute__((cleanup(strCleanup))) = toString(&out);

        FILE *output_file __attribute__((cleanup(fileCleanup))) = fopen(input.ouput_filename, "w");
        fprintf(output_file, "%s", result);

        printf("[TIME] %lf\n", time);
    } else {
        work();
    }

    MPI_Finalize();
    return 0;
}

void work(void)
{
    MPI_Status status;
    int dummy = 0;

    while(1) {
        MPI_Send(&dummy, 1, MPI_INT, 0, VACANT, MPI_COMM_WORLD);

        int sent = 0;
        MPI_Recv(&sent, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if(status.MPI_TAG == STOP) {
            printf("[%d] stopping\n", global.mpi_rank);
            return;
        }

        int task_size = 0;
        MPI_Recv(&task_size, 1, MPI_INT, 0, TASK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("[%d] received sent %d of size %d\n", global.mpi_rank, sent, task_size);

        int a_block[task_size], b_block[task_size];
        MPI_Recv(a_block, task_size, MPI_INT, 0, TASK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(b_block, task_size, MPI_INT, 0, TASK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        int res_block[2][task_size];

        int do_both = ((DIGIT_CULL - 1) == a_block[0] + b_block[0]);
        int carry[2] = {0, 1};
        for(int i = 0; i < task_size; ++i) {
            int tmp = a_block[i] + b_block[i];

            res_block[0][i] = (tmp + carry[0]) % DIGIT_CULL;
            carry[0] = (tmp + carry[0]) / (DIGIT_CULL);
            
            if(!do_both) continue;
            
            res_block[1][i] = (tmp + carry[1]) % DIGIT_CULL;
            carry[1] = (tmp + carry[1]) / (DIGIT_CULL);
        }

        MPI_Send(&sent, 1, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
        MPI_Send(res_block[0], task_size, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
        MPI_Send(res_block[1], task_size, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
        MPI_Send(carry, 2, MPI_INT, 0, RESULT, MPI_COMM_WORLD);
    }
}
