#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

global_t global;

void fromString(longint_t *li, char *raw, int raw_len)
{
    li->len = raw_len / DIGIT_WIDTH;

    for(int i = 0; i < li->len; i++) {
        char *digit_start = raw + raw_len - (i + 1) * DIGIT_WIDTH;
        sscanf(digit_start, "%"STR(DIGIT_WIDTH)"d", &(li->digits[i]));
    }
}

char *toString(longint_t *li)
{
    char *res = calloc(li->len * DIGIT_WIDTH + 1, sizeof(*res));
    REQUIRE(NULL != res, "Memory allocation error");

    int shift = 0;
    sprintf(res, "%d%n", li->digits[li->len - 1], &shift);
    for(int i = 1; i < li->len; ++i) {
        sprintf(res + shift + (i - 1) * DIGIT_WIDTH, "%0*d", DIGIT_WIDTH, li->digits[li->len - i - 1]);
    }
    printf("%s\n", res);
    return res;
}

void parseCli(input_t *input, int *argc, char *(*argv[]))
{
    REQUIRE(2 + 1 == *argc, "Wrong amount of arguments");

    FILE *file_input __attribute__((cleanup(fileCleanup))) = fopen((*argv)[1], "r");
    REQUIRE(NULL != file_input, "can't open input file");

    fscanf(file_input, "%d", &(input->raw_len));
    input->len = input->raw_len / DIGIT_WIDTH;

    char tmp[input->raw_len + 1];
    fscanf(file_input, "%s", tmp);
    fromString(&(input->a), tmp, input->raw_len);
    fscanf(file_input, "%s", tmp);
    fromString(&(input->b), tmp, input->raw_len);

    input->ouput_filename = (*argv)[2];
}

void strCleanup(char **p)
{
    free(*p);
}

void fileCleanup(FILE **p)
{
    fclose(*p);
}
