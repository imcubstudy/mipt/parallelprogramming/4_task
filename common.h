#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>

#ifndef MAX_LONGINT_LEN
#   define MAX_LONGINT_LEN 128
#endif
#ifndef DIGIT_WIDTH
#   define DIGIT_WIDTH 9
#endif
#ifndef DIGIT_CULL
#   define DIGIT_CULL 1000000000
#endif

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define SLINE__ STR(__LINE__)

#define _cmp(op, x, y)                                              \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x op __y ? __x : __y;                                     \
    })
#define min(x, y) _cmp(<, x, y)
#define max(x, y) _cmp(>, x, y)

#define REQUIRE(cond, msg)                                                                  \
    do {                                                                                    \
        if(!(cond)) {                                                                       \
            fprintf(stderr, "ERROR(" SLINE__ "): cond { " #cond " } failed: " msg "\n");    \
            fflush(stderr);                                                                 \
            exit(EXIT_FAILURE);                                                             \
        }                                                                                   \
    } while(0)

typedef struct {
    int mpi_size;
    int mpi_rank;
} global_t;
extern global_t global;

typedef struct {
    int len;
    int digits[MAX_LONGINT_LEN];
} longint_t;

void fromString(longint_t *li, char *raw, int raw_len);

char *toString(longint_t *li);

typedef struct {
    int raw_len;
    int len;

    char *ouput_filename;

    longint_t a;
    longint_t b;
} input_t;

void parseCli(input_t *input, int *argc, char *(*argv[]));

void strCleanup(char **p);
void fileCleanup(FILE **p);

#endif
